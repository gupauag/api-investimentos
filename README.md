Criar uma versão do sistema de simulação de
investimentos que funcione através de uma web API.

Ele deve ter os seguintes endpoints:

GET /investimentos → consulta todos as opções de investimento.

POST /investimentos → cria um novo tipo de investimento.

POST /investimentos/{idInvestimento}/simulacao →realiza a simulação de
rendimento

GET /simulacoes → retorna todas as simulações já feitas.


POST /investimentos → cria um novo tipo de investimento
REQUEST BODY
{
"nome": Poupança,
"redimentoAoMes": 0.2
}
201 CREATED - return
{
“id”: 1,
"nome": Poupança,
"redimentoAoMes": 0.2
}

GET /investimentos → retorna a lista de investimentos possíveis
return - 200 OK
[
{
“id”: 1,
"nome": Poupança,
"redimentoAoMes": 0.2
},
{
“id”: 2,
"nome": OutroInvestimento,
"redimentoAoMes": 2.9
}

POST /investimentos/{idInvestimento}/simulacao → realiza a simulação de
um investimento
REQUEST BODY
{
"nomeInteressado": “Lucas de Melo”,
"email": “lucas.melo@teste.com”,
"valorAplicado": 5000.00,
"quantidadeMeses": 12
}
201 CREATED
{
"redimentoPorMes": 2.00,
"montante": 5150.00
}

GET /simulacoes → retorna todas as simulações já feitas
200 OK
[
{
"nomeInteressado": “Lucas de Melo”,
"email": “lucas.melo@teste.com”,
"valorAplicado": 5000.00,
"quantidadeMeses": 12,
"invetimentoID": 1
}
]

Validações
Devemos garantir que cada simulação salva no banco de
dados tenha um email válido para futuro contato.

Devemos garantir que o formato do valor aplicado e montante
contenha apenas 2 casas depois da vírgula. dica: existe um @
para isso.

Utilizem o @Column(unique = true) no nome do investimento
para garantir que exista apenas um no banco de dados
com aquele nome.