package br.com.apiinvestimentos.Models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "investimento")
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idTipoInvestimento;

    @Column(unique = true)
    private String nome;

    private double rendimentoAoMes;

    public Investimento() {
    }

    public long getIdTipoInvestimento() {
        return idTipoInvestimento;
    }

    public void setIdTipoInvestimento(long idTipoInvestimento) {
        this.idTipoInvestimento = idTipoInvestimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
