package br.com.apiinvestimentos.Models;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "simulacao")
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idSimulacao;

    private String nomeInteressado;

    @Email(message = "O formato do email é inválido")
    @NotNull(message = "O email não pode ser null")
    private String email;

    @Digits(message = "O valor a ser aplicado pode ter apenas 2 casas decimais", integer = 9999, fraction = 2)
    private double valorAplicado;

    private int quantidadeMeses;

    public Simulacao() {
    }

    @ManyToOne(cascade = CascadeType.ALL) //para delete em cascata!
    private Investimento investimentos;


    public Investimento getInvestimentos() {
        return investimentos;
    }

    public void setInvestimentos(Investimento investimentos) {
        this.investimentos = investimentos;
    }


    public long getIdSimulacao() {
        return idSimulacao;
    }

    public void setIdSimulacao(long idSimulacao) {
        this.idSimulacao = idSimulacao;
    }


    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }
}
