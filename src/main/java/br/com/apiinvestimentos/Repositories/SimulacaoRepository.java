package br.com.apiinvestimentos.Repositories;

import br.com.apiinvestimentos.Models.Investimento;
import br.com.apiinvestimentos.Models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository  extends CrudRepository<Simulacao,Long> {
}
