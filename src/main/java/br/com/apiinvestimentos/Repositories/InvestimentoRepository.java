package br.com.apiinvestimentos.Repositories;

import br.com.apiinvestimentos.Models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento,Long> {
}
