package br.com.apiinvestimentos.Controller;

import br.com.apiinvestimentos.DTOs.SimulacaoDTO;
import br.com.apiinvestimentos.Models.Investimento;
import br.com.apiinvestimentos.Models.Simulacao;
import br.com.apiinvestimentos.Service.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    public Investimento incluirInvestimento(@RequestBody Investimento investimento){
        return investimentoService.salvarInvestimento(investimento);
    }

    @GetMapping
    public Iterable<Investimento> exibirInvestimentos(){
        return investimentoService.exibirInvestimentos();
    }

    @PostMapping("/{id}/simulacao")
    public ResponseEntity<SimulacaoDTO> incluirInvestimento(@PathVariable(name = "id") long id, @RequestBody Simulacao simulacao){
        SimulacaoDTO simulacaoDTO = investimentoService.incluirInvestimento(id, simulacao);
        return ResponseEntity.status(201).body(simulacaoDTO);
    }

}