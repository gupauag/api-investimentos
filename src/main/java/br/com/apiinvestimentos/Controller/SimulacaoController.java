package br.com.apiinvestimentos.Controller;


import br.com.apiinvestimentos.Models.Investimento;
import br.com.apiinvestimentos.Models.Simulacao;
import br.com.apiinvestimentos.Service.InvestimentoService;
import br.com.apiinvestimentos.Service.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public Iterable<Simulacao> exibirSimulacoes(){
        return simulacaoService.exibirSimulacoes();
    }

}
