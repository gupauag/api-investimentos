package br.com.apiinvestimentos.DTOs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class SimulacaoDTO {

    private double rendimentoPorMes;
    private double montante;

    public SimulacaoDTO() {
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public String getMontante() {
        DecimalFormat decimal = new DecimalFormat( "0.00" );
        return decimal.format(montante);
    }

    public void setMontante(Double montante) {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
    }
}
