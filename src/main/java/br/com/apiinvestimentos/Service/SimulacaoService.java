package br.com.apiinvestimentos.Service;

import br.com.apiinvestimentos.Models.Investimento;
import br.com.apiinvestimentos.Models.Simulacao;
import br.com.apiinvestimentos.Repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public void salvarSimulacao(Simulacao simulacao){
        Simulacao simul = simulacaoRepository.save(simulacao);
        //return simul;
    }


    public Iterable<Simulacao> exibirSimulacoes(){
        Iterable<Simulacao> simulacaos = simulacaoRepository.findAll();
        return simulacaos;
    }

}
