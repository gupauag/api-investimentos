package br.com.apiinvestimentos.Service;

import br.com.apiinvestimentos.DTOs.SimulacaoDTO;
import br.com.apiinvestimentos.Models.Investimento;
import br.com.apiinvestimentos.Models.Simulacao;
import br.com.apiinvestimentos.Repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento invest = investimentoRepository.save(investimento);
        return invest;
    }

    public Iterable<Investimento> exibirInvestimentos(){
        Iterable<Investimento> produtos = investimentoRepository.findAll();
        return produtos;
    }

    public SimulacaoDTO incluirInvestimento(Long idInvestimento, Simulacao simulacao){

        SimulacaoDTO simulacaoDTO = new SimulacaoDTO();
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(idInvestimento);

        if(!optionalInvestimento.isPresent()) // metodo que verifica se tem ou não o tipo de investimento na consulta dentro do Optional
            throw new RuntimeException("O tipo de investimento não foi encontrado");

        simulacaoDTO.setRendimentoPorMes(optionalInvestimento.get().getRendimentoAoMes());
        //juros compostos = valor * (1 + taxa)^tempo;
        simulacaoDTO.setMontante(simulacao.getValorAplicado() *
                (Math.pow((1 + optionalInvestimento.get().getRendimentoAoMes()), simulacao.getQuantidadeMeses())));

        simulacao.setInvestimentos(optionalInvestimento.get());

        simulacaoService.salvarSimulacao(simulacao);

        return simulacaoDTO;
    }


}
