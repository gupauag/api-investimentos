package br.com.apiinvestimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiinvestimentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiinvestimentosApplication.class, args);
	}

}
